package com.progressoft.evaluationtask;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) throws IOException, NoFileFoundException, ParseFormatException {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println(">> Enter source file location:");
            Path sourcePath = Paths.get(scanner.next());
            System.out.println(">> Enter source file format:");
            String sourceFileFormat = scanner.next();
            System.out.println(">> Enter target file location:");
            Path targetPath = Paths.get(scanner.next());
            System.out.println(">> Enter target file format:");
            String targetFileFormat = scanner.next();
            Parser sourceFactory = Factory.factory(sourceFileFormat);
            Parser targetFactory = Factory.factory(targetFileFormat);
            Comparator comparing = new Comparator(sourceFactory, targetFactory, new FileOutputProducer());
            comparing.compare(sourcePath, targetPath);
        }
    }
}
