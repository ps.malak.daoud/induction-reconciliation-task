<!DOCTYPE html>
<html lang="en">

<head>
<title>Login V16</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/bootstrap/images/icons/favicon.ico" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/vendor/animate/animate.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/vendor/css-hamburgers/hamburgers.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/vendor/animsition/css/animsition.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/vendor/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/vendor/daterangepicker/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/util.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/main.css">
</head>

<body>
<div class="limiter">
<div class="container-login100" style="background-image: url('${pageContext.request.contextPath}/bootstrap/images/bg-01.jpg');">
<div class="wrap-login100 p-t-30 p-b-50">
<span class="login100-form-title p-b-41">
Account Login
</span>
<form class="login100-form validate-form p-b-33 p-t-5">
<div class="wrap-input100 validate-input" data-validate="Enter username">
<input class="input100" type="text" name="username" placeholder="User name">
<span class="focus-input100" data-placeholder="&#xe82a;"></span>
</div>
<div class="wrap-input100 validate-input" data-validate="Enter password">
<input class="input100" type="password" name="pass" placeholder="Password">
<span class="focus-input100" data-placeholder="&#xe80f;"></span>
</div>
<div class="container-login100-form-btn m-t-32">
<button class="login100-form-btn">
Login
</button>
</div>
</form>
</div>
</div>
</div>
<div id="dropDownSelect1"></div>
</form>
<script src="${pageContext.request.contextPath}/bootstrap/vendor/jquery/jquery-3.2.1.min.js" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script src="${pageContext.request.contextPath}/bootstrap/vendor/animsition/js/animsition.min.js" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script src="${pageContext.request.contextPath}/bootstrap/vendor/bootstrap/js/popper.js" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script src="${pageContext.request.contextPath}/bootstrap/vendor/bootstrap/js/bootstrap.min.js" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script src="${pageContext.request.contextPath}/bootstrap/vendor/select2/select2.min.js" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script src="${pageContext.request.contextPath}/bootstrap/vendor/daterangepicker/moment.min.js" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script src="${pageContext.request.contextPath}/bootstrap/vendor/daterangepicker/daterangepicker.js" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script src="${pageContext.request.contextPath}/bootstrap/vendor/countdowntime/countdowntime.js" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/main.js" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="0494bb747166e77cd0e2b99f-text/javascript"></script>
<script type="0494bb747166e77cd0e2b99f-text/javascript">
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
	</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="0494bb747166e77cd0e2b99f-|49" defer=""></script></body>
</html>
