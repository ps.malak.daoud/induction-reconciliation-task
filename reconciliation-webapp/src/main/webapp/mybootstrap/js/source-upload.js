var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    //
    //Button revealed on final step
    document.getElementById("btnCancel").removeAttribute("hidden");
    fillCards();
    //
    document.getElementById("nextBtn").innerHTML = "Compare";
  } else {
    document.getElementById("btnCancel").setAttribute("hidden", true);
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
      document.getElementById("myForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}


function fillCards(){
var sourceFileName = document.getElementById("sourceFileName").value;
var sourceFileType = document.getElementById("sourceFormat").value;
var targetFileName = document.getElementById("targetFileName").value;
var targetFileType = document.getElementById("targetFormat").value;

//window.alert(sourceFileName + " " + sourceFileType+ " " + targetFileName+ " " + targetFileType);

document.getElementById("sourceName").innerHTML = "Name: " +sourceFileName;
document.getElementById("sourceType").innerHTML = "Type: " +sourceFileType;
document.getElementById("targetName").innerHTML = "Name: " +targetFileName;
document.getElementById("targetType").innerHTML = "Type: " +targetFileType;
}

function clearDataAfterCancel(){
 // clearing inputs
 var sourceFileName = document.getElementById("sourceFileName");
 var targetFileName = document.getElementById("targetFileName");
 var sourceFileType = document.getElementById("sourceFormat");
 var targetFileType = document.getElementById("targetFormat");
 var sourceFile = document.getElementById("sourceFile");
 var targetFile = document.getElementById("targetFile");

//clear input type
 sourceFileName.value = '';
 targetFileName.value = '';
 //clear select
 sourceFileType.selectedIndex = 0;
 targetFileType.selectedIndex = 0;
 //clear file type
 sourceFile.value = "";
 targetFile.value = "";
//return to the first page in the form
nextPrev(-2);
    return false;
}