package com.progressoft.evaluationtask.web;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class AuthenticationFilterTest {
    @Test
    public void givenInvalidSession_whenDoFilter_thenRedirectToLogin() throws IOException, ServletException {
        AuthenticationFilter filter = new AuthenticationFilter();
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        FilterChain chain = Mockito.mock(FilterChain.class);
        HttpSession session = Mockito.mock(HttpSession.class);
        FilterConfig mockFilterConfig = mock(FilterConfig.class);

        when(session.getAttribute("username")).thenReturn(null);
        when(session.getAttribute("password")).thenReturn(null);
        when(request.getSession()).thenReturn(session);
        when(request.getContextPath()).thenReturn("/Upload");
        filter.init(mockFilterConfig);
        filter.doFilter(request, response, chain);
        Mockito.verify(response).sendRedirect("/LogIn");
        filter.destroy();
    }
}