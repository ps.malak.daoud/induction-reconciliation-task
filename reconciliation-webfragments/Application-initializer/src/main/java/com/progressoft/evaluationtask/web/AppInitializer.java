package com.progressoft.evaluationtask.web;


import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class AppInitializer implements ServletContainerInitializer {


    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        registerLogInServlet(servletContext);
        registerUploadFileServlet(servletContext);
        registerResultServlet(servletContext);
        registerDownloadServlet(servletContext);
    }

    private void registerLogInServlet(ServletContext ctx) throws ServletException {
        LogInServlet logInServlet = new LogInServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("LogInServlet", logInServlet);
        registration.addMapping("/LogIn");
    }

    private void registerUploadFileServlet(ServletContext ctx) throws ServletException {
        UploadFileServlet uploadFileServlet = new UploadFileServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("UploadFileServlet", uploadFileServlet);
        registration.addMapping("/Upload");
    }

    private void registerResultServlet(ServletContext ctx) throws ServletException {
        ResultServlet resultServlet = new ResultServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("ResultServlet", resultServlet);
        registration.addMapping("/Result");
    }

    private void registerDownloadServlet(ServletContext ctx) throws ServletException {
        DownloadServlet downloadServlet = new DownloadServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("DownloadServlet", downloadServlet);
        registration.addMapping("/Download");
    }
}
