package com.progressoft.evaluationtask.web;

import com.progressoft.evaluationtask.FileWritingException;
import com.progressoft.evaluationtask.OutputHandler;
import com.progressoft.evaluationtask.ParseCSVasList;
import com.progressoft.evaluationtask.WebOutputRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.OngoingStubbing;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.mockito.Mockito.*;

public class ResultServletTest {
    private File newMatchFile = null;
    private File newMisMatchFile = null;
    private File newMissingFile = null;

    @Test
    public void givenCorrectRetrievedPaths_whenCallingResultServlet_thenDispatchToResultJsp() throws Exception {
        ParseCSVasList parse = mock(ParseCSVasList.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        HttpSession httpSession = mock(HttpSession.class);


        createFilesAndWriteData();
        Path matchPath = Paths.get(String.valueOf(newMatchFile));
        Path misMatchPath = Paths.get(String.valueOf(newMisMatchFile));
        Path missingPath = Paths.get(String.valueOf(newMissingFile));

        File newMatchFile = new File(String.valueOf(matchPath));
        File newMisMatchFile = new File(String.valueOf(misMatchPath));
        File newMissingFile = new File(String.valueOf(missingPath));

        when(request.getSession()).thenReturn(httpSession);

        OngoingStubbing<Object> newMatchFile1 = when(request.getSession().getAttribute("newMatchFile")).thenReturn(newMatchFile);
        OngoingStubbing<Object> newMisMatchFile1 = when(request.getSession().getAttribute("newMisMatchFile")).thenReturn(newMisMatchFile);
        OngoingStubbing<Object> newMissingFile1 = when(request.getSession().getAttribute("newMissingFile")).thenReturn(newMissingFile);


        Assertions.assertEquals(httpSession, request.getSession());


        Path matchFile = Paths.get(String.valueOf(newMatchFile1));
        Path misMatchFile = Paths.get(String.valueOf(newMisMatchFile1));
        Path missingFile = Paths.get(String.valueOf(newMissingFile1));

        List<WebOutputRecord> matchedListData = parse.parseCSVasList(matchFile);
        request.setAttribute("matchedListData", matchedListData);

        List<WebOutputRecord> mismatchedListData = parse.parseCSVasList(misMatchFile);
        request.setAttribute("mismatchedListData", mismatchedListData);

        List<WebOutputRecord> missingListData = parse.parseCSVasList(missingFile);
        request.setAttribute("missingListData", missingListData);


        when(request.getRequestDispatcher("/WEB-INF/views/result.jsp")).thenReturn(requestDispatcher);
        new ResultServlet().doGet(request, response);
        verify(requestDispatcher).forward(request, response);
    }

    private void createFilesAndWriteData() throws IOException {
        String tempDirectoryPath = System.getProperty("user.home") + File.separator + "web-reconciliation-results";
        try {
            Files.createDirectories(Paths.get(tempDirectoryPath));
            this.newMatchFile = File.createTempFile("Matching-Transactions-file", ".csv", new File(tempDirectoryPath));
            this.newMisMatchFile = File.createTempFile("Mismatching-Transactions-file", ".csv", new File(tempDirectoryPath));
            this.newMissingFile = File.createTempFile("Missing-Transactions-file", ".csv", new File(tempDirectoryPath));

        } catch (IOException e) {
            throw new FileWritingException("failed to create temp", e);
        }

        OutputHandler outputHandler = new OutputHandler(newMatchFile,newMisMatchFile,newMissingFile);

        String data = "TR-47884222201,140.00,USD,2020-01-20";
        String misData = "SOURCE,TR-47884222202,20.000,JOD,2020-01-22";
        String misData2 = "TARGET,TR-47884222202,30.000,JOD,2020-01-22";

        outputHandler.writeMatched(data);
        outputHandler.writeUnmatched(misData, misData2);
        outputHandler.writeMissing(misData2);
    }


}
