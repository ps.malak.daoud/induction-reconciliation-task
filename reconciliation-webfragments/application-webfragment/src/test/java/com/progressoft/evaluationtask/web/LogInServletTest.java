package com.progressoft.evaluationtask.web;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.Mockito.*;

public class LogInServletTest {

    @Test
    public void givenCorrectLogIn_whenCallingLogInServlet_thenReturnLogInJspPage() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher("/WEB-INF/views/LogIn.jsp")).thenReturn(requestDispatcher);
        new LogInServlet().doGet(request, response);
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void givenServlet_whenTestServlet_thenCorrect() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        when(request.getParameter("username")).thenReturn("admin");
        when(request.getParameter("password")).thenReturn("admin123");
        when(request.getSession()).thenReturn(session);
        new LogInServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("username");
        verify(request, atLeast(1)).getParameter("password");
        verify(session).setAttribute("username", "admin");
    }
}

