package com.progressoft.evaluationtask.web;


import org.junit.jupiter.api.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class UploadFileServletTest {

    @Test
    public void givenCorrectSourceUploadJsp_whenCallingUploadFileServlet_thenReturnSourceUploadJsp() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher("/WEB-INF/views/source-upload.jsp")).thenReturn(requestDispatcher);
        new UploadFileServlet().doGet(request, response);
        verify(requestDispatcher).forward(request, response);
    }

//    @Test
//    public void givenCorrectInfo_whenUploadingFiles_thenCorrect() throws IOException {
//        HttpServletRequest request = mock(HttpServletRequest.class);
//        HttpServletResponse response = mock(HttpServletResponse.class);
//        HttpSession session = mock(HttpSession.class);
//
//        when(request.getSession()).thenReturn(session);
//        new UploadFileServlet().doPost(request, response);
//
//    }
}
