package com.progressoft.evaluationtask.web;


import com.progressoft.evaluationtask.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class ResultServlet extends HttpServlet {
    ParseCSVasList parse = new ParseCSVasList();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        parseCSVasListForResultDisplay(request);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/result.jsp");
        requestDispatcher.forward(request, response);
    }

    private void parseCSVasListForResultDisplay(HttpServletRequest request) throws IOException {
        Path matchFile = Paths.get(request.getSession().getAttribute("newMatchFile").toString());
        Path misMatchFile = Paths.get(request.getSession().getAttribute("newMisMatchFile").toString());
        Path missingFile = Paths.get(request.getSession().getAttribute("newMissingFile").toString());

        List<WebOutputRecord> matchedListData = parse.parseCSVasList(matchFile);
        request.setAttribute("matchedListData", matchedListData);

        List<WebOutputRecord> mismatchedListData = parse.parseCSVasList(misMatchFile);
        request.setAttribute("mismatchedListData", mismatchedListData);

        List<WebOutputRecord> missingListData = parse.parseCSVasList(missingFile);
        request.setAttribute("missingListData", missingListData);
    }

}

