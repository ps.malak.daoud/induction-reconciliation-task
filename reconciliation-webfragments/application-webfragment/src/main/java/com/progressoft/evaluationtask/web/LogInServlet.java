package com.progressoft.evaluationtask.web;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogInServlet extends HttpServlet {


    public LogInServlet() {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwardToLoginView(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String error = "";
        if (password.equals("admin123") && username.equals("admin")) {
            HttpSession session = req.getSession();
            session.setAttribute("username", username);
            resp.sendRedirect("/Upload");
        } else {
            req.setAttribute("error", error);
            forwardToLoginView(req, resp);
        }
    }

    private void forwardToLoginView(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/LogIn.jsp");
        requestDispatcher.forward(req, resp);
    }
}
