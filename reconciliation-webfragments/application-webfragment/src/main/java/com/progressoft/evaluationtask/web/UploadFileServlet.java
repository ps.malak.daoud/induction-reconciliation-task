package com.progressoft.evaluationtask.web;

import com.progressoft.evaluationtask.*;
import com.progressoft.evaluationtask.Comparator;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class UploadFileServlet extends HttpServlet {

    private static final String UPLOAD_DIRECTORY = "upload";
    private static final int THRESHOLD_SIZE = 1024 * 1024 * 3;
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 40;
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50;
    private File newMatchFile, newMisMatchFile, newMissingFile;
    private String tempDirectoryPath;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/source-upload.jsp");
        requestDispatcher.forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        if (!ServletFileUpload.isMultipartContent(request)) {
            PrintWriter writer = response.getWriter();
            writer.println("Request does not contain upload data");
            writer.flush();
            return;
        }
        // TODO too much long method //done
        tempFilesAndDirCreation();
        setFileSessionAttribute(request, newMatchFile, newMisMatchFile, newMissingFile);
        ServletFileUpload upload = getServletFileUpload();
        String uploadDirPath = constructDirPath();
        // TODO you are setting unused attributes //done - deleted the unused line
        Map<String, String> parametersHashMap = new HashMap();
        try {
            prepareResultData(request, upload, uploadDirPath, parametersHashMap);
        } catch (Exception ex) {
            dispatchResult(request, response, ex);
            return;
        }
        reconcile(parametersHashMap);
        response.sendRedirect("/Result");
    }

    private void reconcile(Map<String, String> parametersHashMap) throws IOException {
        Path sourcePath = Paths.get(parametersHashMap.get("sourceFile"));
        String sourceFormat = parametersHashMap.get("sourceFormat");
        Path targetPath = Paths.get(parametersHashMap.get("targetFile"));
        String targetFormat = parametersHashMap.get("targetFormat");
        reconciliation(newMatchFile, newMisMatchFile, newMissingFile, sourcePath, sourceFormat, targetPath, targetFormat);
    }

    private void dispatchResult(HttpServletRequest request, HttpServletResponse response, Exception ex) throws ServletException, IOException {
        request.setAttribute("message", "There was an error: " + ex.getMessage());
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/source-upload.jsp");
        requestDispatcher.forward(request, response);
    }

    private void tempFilesAndDirCreation() {
        tempDirectoryPath = System.getProperty("user.home") + File.separator + "reconciliation-results";
        try {
            Path directory = Files.createDirectories(Paths.get(tempDirectoryPath));
            newMatchFile = File.createTempFile("Matching-Transactions-file", ".csv", new File(String.valueOf(directory)));
            newMisMatchFile = File.createTempFile("Mismatching-Transactions-file", ".csv", new File(String.valueOf(directory)));
            newMissingFile = File.createTempFile("Missing-Transactions-file", ".csv", new File(String.valueOf(directory)));

        } catch (IOException e) {
            throw new FileWritingException("failed to create temp", e);
        }
    }

    private void prepareResultData(HttpServletRequest request, ServletFileUpload upload, String uploadDirPath, Map<String, String> parametersHashMap) throws Exception {
        List formItems = upload.parseRequest(request);
        Iterator iter = formItems.iterator();
        while (iter.hasNext()) {
            FileItem item = (FileItem) iter.next();
            if (!item.isFormField()) {
                String fileName = new File(item.getName()).getName();
                String filePath = uploadDirPath + File.separator + fileName;
                File storeFile = new File(filePath);
                item.write(storeFile);
                parametersHashMap.put(item.getFieldName(), filePath);
            } else {
                parametersHashMap.put(item.getFieldName(), item.getString());
            }
        }
    }

    private void setFileSessionAttribute(HttpServletRequest request, File newMatchFile, File newMisMatchFile, File newMissingFile) {
        HttpSession session = request.getSession();
        session.setAttribute("newMatchFile", newMatchFile);
        session.setAttribute("newMisMatchFile", newMisMatchFile);
        session.setAttribute("newMissingFile", newMissingFile);
    }

    private void reconciliation(File newMatchFile, File newMisMatchFile, File newMissingFile, Path sourcePath, String sourceFormat, Path targetPath, String targetFormat) throws IOException {
        Parser sourceFactory = Factory.factory(sourceFormat);
        Parser targetFactory = Factory.factory(targetFormat);
        Comparator comparing = new Comparator(sourceFactory, targetFactory, new OutputHandler(newMatchFile, newMisMatchFile, newMissingFile));
        comparing.compare(sourcePath, targetPath);
    }

    private String constructDirPath() {
        String uploadDirPath = getServletContext().getRealPath("")
                + File.separator + UPLOAD_DIRECTORY;
        File uploadDir = new File(uploadDirPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdirs();
        }
        return uploadDirPath;
    }

    private ServletFileUpload getServletFileUpload() {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(THRESHOLD_SIZE);
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(MAX_FILE_SIZE);
        upload.setSizeMax(MAX_REQUEST_SIZE);
        return upload;
    }

}


