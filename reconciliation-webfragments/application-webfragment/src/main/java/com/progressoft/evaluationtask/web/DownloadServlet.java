package com.progressoft.evaluationtask.web;

import com.progressoft.evaluationtask.CsvToJson;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DownloadServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String format = request.getParameter("ext");
        String targetFile = request.getParameter("targetFile");
        Path filePath = getTargetPath(request, targetFile);
        responseForDownload(response, format, targetFile);
        filePath = getJsonPathToDownload(format, filePath);
        download(response, format, filePath);
    }

    private Path getTargetPath(HttpServletRequest request, String targetFile) {
        Path matchFile = Paths.get(request.getSession().getAttribute("newMatchFile").toString());
        Path misMatchFile = Paths.get(request.getSession().getAttribute("newMisMatchFile").toString());
        Path missingFile = Paths.get(request.getSession().getAttribute("newMissingFile").toString());

        return getPathToDownload(matchFile, misMatchFile, missingFile, targetFile);
    }

    private String generateJsonFile(String path) {
        CsvToJson csvToJsonConverter = new CsvToJson();
        return csvToJsonConverter.convert(path);
    }

    private Path getPathToDownload(Path matchFile, Path misMatchFile, Path missingFile, String targetFile) {
        Path filePath;
        switch (targetFile) {
            case "Match":
                filePath = matchFile;
                break;
            case "misMatch":
                filePath = misMatchFile;
                break;
            case "Missing":
                filePath = missingFile;
                break;
            default:
                throw new IllegalArgumentException("Illegal File Name");
        }
        return filePath;
    }

    private void download(HttpServletResponse response, String format, Path filePath) throws IOException {
        PrintWriter writer = response.getWriter();
        BufferedReader br = new BufferedReader(new FileReader(filePath.toString()));
        try {
            String line;
            while ((line = br.readLine()) != null) {
                writer.write(line);
                if (format.equals("csv"))
                    writer.write("\n");
            }
        } finally {
            br.close();
        }
    }

    private Path getJsonPathToDownload(String format, Path filePath) {
        if (format.equals("json")) {
            filePath = Paths.get(generateJsonFile(String.valueOf(filePath)));
        }
        return filePath;
    }

    private void responseForDownload(HttpServletResponse response, String format, String targetFile) {
        response.reset();
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=\"" +
                String.format("%s.%s", targetFile, format) + "\"");
    }
}
