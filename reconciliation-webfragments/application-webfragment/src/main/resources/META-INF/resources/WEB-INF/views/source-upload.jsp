<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <title>Upload File</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/mybootstrap/css/source-upload.css">
</head>
<body style="background-image: url('${pageContext.request.contextPath}/mybootstrap/images/blue-background.jpg');">

<form id="myForm" action="${pageContext.request.contextPath}/Upload" method="post" enctype="multipart/form-data" class="responsive"
      style="background-image: url('${pageContext.request.contextPath}/mybootstrap/images/Grey-Background.jpg');">

    <!-- One "tab" for each step in the form: -->
    <div class="tab">
        <h1>
            Source file Upload:
        </h1>
        <div> Source Name :
            <p><input id="sourceFileName" name="sourceFileName" type="text"></p>
        </div>
        <div> File Type :
            <p>
            <div class="form-holder">
                <select name="sourceFormat" id="sourceFormat" class="form-control">
                    <option value="csv" class="option">csv</option>
                    <option value="Json" class="option">Json</option>
                </select>
            </div>
            </p>
        </div>
        <div> Choose File to Upload :
            <p><input id="sourceFile" type="file" name="sourceFile"/></p>
        </div>
    </div>
    <div class="tab">
        <h1>
            Target file Upload:
        </h1>
        <div> Target Name :
            <p><input id="targetFileName" name="targetFileName" type="text"></p>
        </div>
        <div> File Type :
            <p>
            <div class="form-holder">
                <select name="targetFormat" id="targetFormat" class="form-control">
                    <option value="csv" class="option">csv</option>
                    <option value="Json" class="option">Json</option>
                </select>
            </div>
            </p>
        </div>
        <div> Choose File to Upload :
            <p><input id="targetFile" type="file" name="targetFile"/></p>
        </div>
    </div>
    <div class="tab">
        <h1>
            Summary:
        </h1>
        <p>
            <div class="row">
                <div class="column">
                    <div class="card">
                        <h4><b>Source</b></h4>
                            <p id="sourceName">Name: </p>
                            <p id="sourceType">Type: </p>
                    </div>
                </div>

                <div class="column">
                    <div class="card">
                        <h4><b>Target</b></h4>
                        <p id="targetName">Name: </p>
                        <p id="targetType">Type: </p>
                    </div>
                </div>
             </div>
    </p>
    <div> Result Files Format :
        <p>
        <div class="form-holder">
            <select name="resultFormat" id="resultFormat" class="form-control">
                <option value="csv" class="option">csv</option>
                <option value="Json" class="option">Json</option>
            </select>
        </div>
        </p>
    </div>
    </div>

    <div style="overflow:auto;">
        <div style="float:right;">
            <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
            <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
            <button hidden type="button" id="btnCancel" onclick="clearDataAfterCancel()">Cancel</button>
        </div>
    </div>
    <!-- Circles which indicates the steps of the form: -->
    <div style="text-align:center;margin-top:40px;">
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
    </div>
</form>

<script src="${pageContext.request.contextPath}/mybootstrap/js/source-upload.js"></script>


</body>
</html>



