<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>


<jip:login-head/>

<div class="limiter">
    <div class="container-login100"
         style="background-image: url('${pageContext.request.contextPath}/bootstrap/images/bg-01.jpg');">
        <div class="wrap-login100 p-t-30 p-b-50">
<span class="login100-form-title p-b-41">
Account Login
</span>

            <% if (request.getAttribute("error") != null) { %>
            <div class="alert">
                <strong>you entered a wrong username or password. Please try
                    again<br/></strong>
                <%= request.getAttribute("error")%>
            </div>
            <% } %>


            <form action="${pageContext.request.contextPath}/LogIn" class="login100-form validate-form p-b-33 p-t-5"
                  method="POST">
                <div class="wrap-input100 validate-input" data-validate="Enter username">
                    <input class="input100" type="text" name="username" id="username" placeholder="User name">
                    <span class="focus-input100" data-placeholder="&#xe82a;"></span>
                </div>
                <div class="wrap-input100 validate-input" data-validate="Enter password">
                    <input class="input100" type="password" name="password" id="password" placeholder="Password">
                    <span class="focus-input100" data-placeholder="&#xe80f;"></span>
                </div>
                <div class="container-login100-form-btn m-t-32">
                    <button id="login" class="login100-form-btn">
                        Login
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="dropDownSelect1"></div>
</form>

<jip:login-end/>
