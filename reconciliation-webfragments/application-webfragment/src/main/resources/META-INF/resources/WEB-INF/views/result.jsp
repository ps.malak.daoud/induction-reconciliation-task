<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reconciliation Results</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}mybootstrap/css/result.css">
    <script src="${pageContext.request.contextPath}mybootstrap/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}mybootstrap/jquery/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Reconciliation Results</h2>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Matching</a></li>
        <li><a data-toggle="tab" href="#menu1">Mis-Matching</a></li>
        <li><a data-toggle="tab" href="#menu2">Missing</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <h3>Matching Transactions</h3>
            <div id="matchingData">
                <table class="table" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>transaction id</th>
                        <th>amount</th>
                        <th>currecny code</th>
                        <th>value date</th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:forEach var="element" items="${matchedListData}">
                        <c:set var="count" value="0" scope="session"/>
                        <tr>
                            <td><c:out value="${count + 1}"/></td>
                            <td>${element.transactionId}</td>
                            <td>${element.amount}</td>
                            <td>${element.currency}</td>
                            <td>${element.date}</td>
                        </tr>
                    </c:forEach>

                    </tbody>
                </table>
            </div>
            <div class='button'>
                <div class="form-holder">
                    <form method="get" action="${pageContext.request.contextPath}/Download" id="matchedForm">
                        <input type="hidden" value="Match" name="targetFile"/>
                        <select name="ext" class="form-control"
                                onChange="javascript:getElementById('matchedForm').submit();">
                            <option>Download as :</option>
                            <option value="csv" class="option">csv</option>
                            <option value="json" class="option">Json</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>
        <div id="menu1" class="tab-pane fade">
            <h3>Mis-Matching Transactions</h3>
            <div id="misMatchData">
                <table class="table" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>found in</th>
                        <th>transaction id</th>
                        <th>amount</th>
                        <th>currecny code</th>
                        <th>value date</th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:forEach var="element" items="${mismatchedListData}">
                        <c:set var="count" value="0" scope="session"/>
                        <tr>
                            <td><c:out value="${count + 1}"/></td>
                            <td>${element.foundIn}</td>
                            <td>${element.transactionId}</td>
                            <td>${element.amount}</td>
                            <td>${element.currency}</td>
                            <td>${element.date}</td>
                        </tr>
                    </c:forEach>

                    </tbody>
                </table>
            </div>
            <div class='button'>
                <div class="form-holder">
                    <form method="get" action="${pageContext.request.contextPath}/Download" id="missMatchedForm">
                        <input type="hidden" value="misMatch" name="targetFile"/>
                        <select name="ext" class="form-control"
                                onChange="javascript:getElementById('missMatchedForm').submit();">
                            <option>Download as :</option>
                            <option value="csv" class="option">csv</option>
                            <option value="json" class="option">Json</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>
        <div id="menu2" class="tab-pane fade">
            <h3>Missing Transactions</h3>
            <div id="missingData">
                <table class="table" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>found in</th>
                        <th>transaction id</th>
                        <th>amount</th>
                        <th>currecny code</th>
                        <th>value date</th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:forEach var="element" items="${missingListData}">
                        <c:set var="count" value="0" scope="session"/>
                        <tr>
                            <td><c:out value="${count + 1}"/></td>
                            <td>${element.foundIn}</td>
                            <td>${element.transactionId}</td>
                            <td>${element.amount}</td>
                            <td>${element.currency}</td>
                            <td>${element.date}</td>
                        </tr>
                    </c:forEach>

                    </tbody>
                </table>
            </div>

            <div class='button'>
                <div class="form-holder">
                    <form method="get" action="${pageContext.request.contextPath}/Download" id="missingForm">
                        <input type="hidden" value="Missing" name="targetFile"/>
                        <select name="ext" class="form-control"
                                onChange="javascript:getElementById('missingForm').submit();">
                            <option>Download as :</option>
                            <option value="csv" class="option">csv</option>
                            <option value="json" class="option">Json</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/Upload" method="GET">
        <input type="submit" value="Compare new Files">
    </form>
</div>


<script type="text/javascript" src="${pageContext.request.contextPath}/js/read-csv.js"></script>

</body>
</html>
