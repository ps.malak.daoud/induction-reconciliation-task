package com.progressoft.evaluationtask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class OutputHandlerTest {
    private File newMatchFile = null;
    private File newMisMatchFile = null;
    private File newMissingFile = null;
    @Test
    public void givenEmptyString_whenWritingMatched_thenThrowNullPointerException() {
        File newMatchFile = null;
        File newMisMatchFile = null;
        File newMissingFile = null;
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        OutputHandler outputHandler = new OutputHandler(newMatchFile, newMisMatchFile, newMissingFile);
                        outputHandler.writeMatched(null);
                    }
                });
        Assertions.assertEquals(null, nullPointerException.getMessage());
    }

    @Test
    public void givenEmptyString_whenWritingMissMatched_thenThrowNullPointerException() {
        File newMatchFile = null;
        File newMisMatchFile = null;
        File newMissingFile = null;
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        OutputHandler outputHandler = new OutputHandler(newMatchFile, newMisMatchFile, newMissingFile);
                        outputHandler.writeUnmatched(null, null);
                    }
                });
        Assertions.assertEquals(null, nullPointerException.getMessage());
    }

    @Test
    public void givenEmptyString_whenWritingMissing_thenThrowNullPointerException() {
        File newMatchFile = null;
        File newMisMatchFile = null;
        File newMissingFile = null;
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        OutputHandler outputHandler = new OutputHandler(newMatchFile, newMisMatchFile, newMissingFile);
                        outputHandler.writeMissing(null);
                    }
                });
        Assertions.assertEquals(null, nullPointerException.getMessage());
    }
    @Test
    public void givenCorrectData_whenHandlingOutput_thenAssertTrue() throws IOException {

        String tempDirectoryPath = System.getProperty("user.home") + File.separator + "web-reconciliation-results";
        try {
            Files.createDirectories(Paths.get(tempDirectoryPath));
            this.newMatchFile = File.createTempFile("Matching-Transactions-file", ".csv", new File(tempDirectoryPath));
            this.newMisMatchFile = File.createTempFile("Mismatching-Transactions-file", ".csv", new File(tempDirectoryPath));
            this.newMissingFile = File.createTempFile("Missing-Transactions-file", ".csv", new File(tempDirectoryPath));

        } catch (IOException e) {
            throw new FileWritingException("failed to create temp", e);
        }

        OutputHandler outputHandler = new OutputHandler(newMatchFile,newMisMatchFile,newMissingFile);

        String data = "TR-47884222201,140.00,USD,2020-01-20";
        String misData = "SOURCE,TR-47884222202,20.000,JOD,2020-01-22";
        String misData2 = "TARGET,TR-47884222202,30.000,JOD,2020-01-22";

        outputHandler.writeMatched(data);
        outputHandler.writeUnmatched(misData, misData2);
        outputHandler.writeMissing(misData2);

        Path matchPath = Paths.get(String.valueOf(newMatchFile));
        Path misMatchPath = Paths.get(String.valueOf(newMisMatchFile));
        Path missingPath = Paths.get(String.valueOf(newMissingFile));

        String matchingFileResult;
        String misMatchingFileResult;
        String missingFileResult;

        matchingFileResult = returnMatchingFileResult(matchPath);
        misMatchingFileResult = returnMisMatchingFileResult(misMatchPath);
        missingFileResult = returnMissingFileResult(missingPath);

        Assertions.assertEquals(matchingFileResult, data);
        Assertions.assertEquals(misMatchingFileResult, misData);
        Assertions.assertEquals(missingFileResult, misData2);
    }

    private String returnMissingFileResult(Path missingPath) throws IOException {
        String missingFileResult;
        try (BufferedReader br = new BufferedReader(new FileReader(missingPath.toString()))) {
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);

            }
            missingFileResult = lines.get(1);
        }
        return missingFileResult;
    }

    private String returnMisMatchingFileResult(Path misMatchPath) throws IOException {
        String misMatchingFileResult;
        try (BufferedReader br = new BufferedReader(new FileReader(misMatchPath.toString()))) {
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);

            }
            misMatchingFileResult = lines.get(1);
        }
        return misMatchingFileResult;
    }

    private String returnMatchingFileResult(Path matchPath) throws IOException {
        String matchingFileResult;
        try (BufferedReader br = new BufferedReader(new FileReader(matchPath.toString()))) {
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);

            }
            matchingFileResult = lines.get(1);
        }
        return matchingFileResult;
    }
}