
package com.progressoft.evaluationtask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ComparatorTest {
    @Test
    public void givenBothNullPaths_whenComparingFiles_thenThrowNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> {
                    new Comparator(null, null, null);
                });

        Assertions.assertEquals("null source and target Parsers", nullPointerException.getMessage());
    }

    @Test
    public void givenNullParsers_whenComparing_thenAssertNotNull() throws NoFileFoundException, IOException, ParseFormatException {
        Path sourcePath = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        Path targetPath = Paths.get("../sample-files/input-files/bank-transactions.csv");
        Parser sourceFactory = Factory.factory("json");
        Parser targetFactory = Factory.factory("csv");
        Assertions.assertNotNull(sourceFactory);
        Assertions.assertNotNull(targetFactory);
    }

    @Test
    public void givenCorrectData_whenComparing_thenAssertTrue() throws NoFileFoundException, IOException, ParseFormatException {
        // TODO you should verify the generated files //done
        Path sourcePath = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        Path targetPath = Paths.get("../sample-files/input-files/bank-transactions.csv");
        Parser sourceFactory = Factory.factory("json");
        Parser targetFactory = Factory.factory("csv");
        Comparator comparing = new Comparator(sourceFactory, targetFactory, new FileOutputProducer());
        Assertions.assertNotNull(sourceFactory);
        Assertions.assertNotNull(targetFactory);
        HashMap<String, Record> sourceRecords = sourceFactory.read(sourcePath);
        HashMap<String, Record> targetRecords = targetFactory.read(targetPath);
        Assertions.assertNotNull(sourceRecords);
        Assertions.assertNotNull(targetRecords);
        comparing.compare(sourcePath,targetPath);//this what gives the class full coverage

        String match = "TR-47884222206,500.0,USD,2020-02-10";
        String misMatch = "TARGET,TR-47884222202,30.000,JOD,2020-01-22";
        String missing = "SOURCE,TR-47884222204,1200,JOD,2020-01-31";

        Path matchPath = Paths.get("../reconciliation-results/Matching-Transactions-file.csv");
        Path misMatchPath = Paths.get("../reconciliation-results/Mismatching-Transactions-file.csv");
        Path missingPath = Paths.get("../reconciliation-results/Missing-Transactions-file.csv");

        String matchingFileResult;
        String misMatchingFileResult;
        String missingFileResult;

        matchingFileResult = returnMatchingFileResult(matchPath);
        misMatchingFileResult = returnMisMatchingFileResult(misMatchPath);
        missingFileResult = returnMissingFileResult(missingPath);

        Assertions.assertEquals(matchingFileResult, match);
        Assertions.assertEquals(misMatchingFileResult, misMatch);
        Assertions.assertEquals(missingFileResult, missing);

    }

    private String returnMissingFileResult(Path missingPath) throws IOException {
        String missingFileResult;
        try (BufferedReader br = new BufferedReader(new FileReader(missingPath.toString()))) {
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);

            }
            missingFileResult = lines.get(1);
        }
        return missingFileResult;
    }

    private String returnMisMatchingFileResult(Path misMatchPath) throws IOException {
        String misMatchingFileResult;
        try (BufferedReader br = new BufferedReader(new FileReader(misMatchPath.toString()))) {
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);

            }
            misMatchingFileResult = lines.get(1);
        }
        return misMatchingFileResult;
    }

    private String returnMatchingFileResult(Path matchPath) throws IOException {
        String matchingFileResult;
        try (BufferedReader br = new BufferedReader(new FileReader(matchPath.toString()))) {
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);

            }
            matchingFileResult = lines.get(1);
        }
        return matchingFileResult;
    }
}
