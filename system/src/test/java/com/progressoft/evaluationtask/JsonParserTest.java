
package com.progressoft.evaluationtask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JsonParserTest {
    private File matchFile;

    @Test
    public void givenNullPath_whenParsingFile_thenThrowNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> {
                    JsonParser jsonParser = new JsonParser();
                    jsonParser.read(null);
                });
        Assertions.assertEquals("null path", nullPointerException.getMessage());
    }

    @Test
    public void givenNonExistPath_whenReadingFile_thenThrowFileNotFoundException() {
        Path path = Paths.get("test100");
        Assertions.assertThrows(NoFileFoundException.class, () -> {
            JsonParser jsonParser = new JsonParser();
            jsonParser.read(path);
        });
    }

    @Test
    public void givenDirectory_whenReadingFile_thenThrowIllegalArgumentException() {
        Path path = Paths.get(".");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    JsonParser jsonParser = new JsonParser();
                    jsonParser.read(path);
                });
        Assertions.assertEquals("Not file", exception.getMessage());
    }

    // TODO where is the happy scenarios //done
    @Test
    public void givenCorrectData_whenParsingJsonFile_thenAssertTrue() throws IOException {
        String data = "null,TR-47884222206,500.00,USD,2020-02-10";
        String result = "";
        String tempDirectoryPath = System.getProperty("user.home") + File.separator + "console-reconciliation-results";
        try {
            Path dirPath = Paths.get(tempDirectoryPath);
            validateDirPath(String.valueOf(dirPath));
            this.matchFile = File.createTempFile("Matching-Transactions-file", ".json", new File(tempDirectoryPath));
            if (!(matchFile.exists())) {
                matchFile.createNewFile();
            }
            System.out.println();
        } catch (IOException e) {
            throw new NoFileFoundException("Error while validating Directory or File paths");
        }
        Path path = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        JsonParser jsonParser = new JsonParser();
        HashMap<String, Record> read = jsonParser.read(path);

        Iterator<String> iterator = read.keySet().iterator();
        Record value = null;
        if (iterator.hasNext()) {
            value = read.get(iterator.next());
        }
        result = result + value;
        Assertions.assertEquals(result, data);
    }


    private void validateDirPath(String uploadFolder) throws IOException {
        if (!Files.exists(Paths.get(uploadFolder))) {
            Files.createDirectory(Paths.get(uploadFolder));
        }
    }

}
