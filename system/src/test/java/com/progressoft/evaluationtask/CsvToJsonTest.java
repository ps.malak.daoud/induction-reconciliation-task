package com.progressoft.evaluationtask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class CsvToJsonTest {
    @Test
    public void givenCorrectInputPathString_whenconvertingCsvToJson_thenCorrect(){
        CsvToJson csvToJson = new CsvToJson();
        String fileNewExtension = ".json";
        Path inputPath = Paths.get("../sample-files/input-files/bank-transactions.csv");
        String convert = csvToJson.convert(String.valueOf(inputPath));
        Assertions.assertEquals(convert,fileNewExtension);

    }
}
