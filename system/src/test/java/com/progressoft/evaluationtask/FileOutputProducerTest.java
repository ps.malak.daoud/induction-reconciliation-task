package com.progressoft.evaluationtask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class FileOutputProducerTest {
    @Test
    public void givenEmptyString_whenWritingMatched_thenThrowNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> {
                    FileOutputProducer fileOutputProducer = new FileOutputProducer();
                    fileOutputProducer.writeMatched(null);
                });
        Assertions.assertEquals(null, nullPointerException.getMessage());
    }

    @Test
    public void givenEmptyString_whenWritingMissMatched_thenThrowNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> {
                    FileOutputProducer fileOutputProducer = new FileOutputProducer();
                    fileOutputProducer.writeUnmatched(null, null);
                });
        Assertions.assertEquals(null, nullPointerException.getMessage());
    }

    @Test
    public void givenEmptyString_whenWritingMissing_thenThrowNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> {
                    FileOutputProducer fileOutputProducer = new FileOutputProducer();
                    fileOutputProducer.writeMissing(null);
                });
        Assertions.assertEquals(null, nullPointerException.getMessage());
    }

    // TODO no happy scenarios //Done
    @Test
    public void givenCorrectData_whenProducingOutput_thenAssertTrue() throws IOException {
        FileOutputProducer fileOutputProducer = new FileOutputProducer();

        String data = "TR-47884222206,500.0,USD,2020-02-10";
        String misData = "TARGET,TR-47884222202,30.000,JOD,2020-01-22";
        String misData2 = "SOURCE,TR-47884222204,1200,JOD,2020-01-31";

        fileOutputProducer.writeMatched(data);
        fileOutputProducer.writeUnmatched(misData, misData2);
        fileOutputProducer.writeMissing(misData2);

        Path matchPath = Paths.get("../reconciliation-results/Matching-Transactions-file.csv");
        Path misMatchPath = Paths.get("../reconciliation-results/Mismatching-Transactions-file.csv");
        Path missingPath = Paths.get("../reconciliation-results/Missing-Transactions-file.csv");

        String matchingFileResult;
        String misMatchingFileResult;
        String missingFileResult;

        matchingFileResult = returnMatchingFileResult(matchPath);
        misMatchingFileResult = returnMisMatchingFileResult(misMatchPath);
        missingFileResult = returnMissingFileResult(missingPath);

        Assertions.assertEquals(matchingFileResult, data);
        Assertions.assertEquals(misMatchingFileResult, misData);
        Assertions.assertEquals(missingFileResult, misData2);
    }

    private String returnMissingFileResult(Path missingPath) throws IOException {
        String missingFileResult;
        try (BufferedReader br = new BufferedReader(new FileReader(missingPath.toString()))) {
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);

            }
            missingFileResult = lines.get(1);
        }
        return missingFileResult;
    }

    private String returnMisMatchingFileResult(Path misMatchPath) throws IOException {
        String misMatchingFileResult;
        try (BufferedReader br = new BufferedReader(new FileReader(misMatchPath.toString()))) {
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);

            }
            misMatchingFileResult = lines.get(1);
        }
        return misMatchingFileResult;
    }

    private String returnMatchingFileResult(Path matchPath) throws IOException {
        String matchingFileResult;
        try (BufferedReader br = new BufferedReader(new FileReader(matchPath.toString()))) {
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);

            }
            matchingFileResult = lines.get(1);
        }
        return matchingFileResult;
    }
}
