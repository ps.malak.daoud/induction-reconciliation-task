package com.progressoft.evaluationtask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FactoryTest {

    @Test
    public void givenNullFormat_whenFactory_thenThrowNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> Factory.factory(null));
        Assertions.assertEquals("null file format", nullPointerException.getMessage());
    }
    @Test
    public void givenUnknownFormat_whenCheckingCorrectFormat_thenThrowIllegalStateException() {
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class,
                () -> Factory.factory("null"));
        Assertions.assertEquals("invalid format", illegalArgumentException.getMessage());
    }

}
