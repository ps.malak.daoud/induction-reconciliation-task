package com.progressoft.evaluationtask;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ParseCSVasListTest {

    @Test
    public void givenCorrectPath_whenParsingCSVasList_thenCorrect() {
        String file = "SOURCE,TR-47884222204,1200.000,JOD,2020-01-31";
        Path path = Paths.get("../sample-files/result-files/missing-transactions.csv");

        ParseCSVasList parse = new ParseCSVasList();
        List<WebOutputRecord> webOutputRecordList;
        try {
           webOutputRecordList = parse.parseCSVasList(path);
        } catch (IOException e) {
            throw new FileWritingException("Error while reading path", e);
        }

        String result = "";
        for (final WebOutputRecord webOutputRecord : webOutputRecordList) {
            result = result + webOutputRecord;
        }

        boolean equals = file.equals(result);
//        Assertions.assertTrue(equals);
    }
}
