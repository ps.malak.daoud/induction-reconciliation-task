package com.progressoft.evaluationtask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;

public class WebOutputRecordTest {
    @Test
    public void givenNullData_whenFillingFileObject_thenThrowNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new WebOutputRecord(null, null, null, null));
        Assertions.assertEquals("Null Data Entered", nullPointerException.getMessage());
    }

    @Test
    public void givenString_whenCallingToString_thenTrue() {
        WebOutputRecord record = new WebOutputRecord();
        String expected = record.toString();
        Assertions.assertEquals(expected, record.toString());

    }

    @Test
    public void givenString_whenCallingMatchToString_thenTrue(){
        WebOutputRecord record = new WebOutputRecord();
        String expected = record.matchToString();
        Assertions.assertEquals(expected, record.matchToString());

    }

    @Test
    public void givenFullData_whenFillingFileObject_thenTrue() {
        String string = "null";
        BigDecimal bigDecimal = new BigDecimal("123");
        String string1 = "null";
        Date date = new Date();
        WebOutputRecord record = new WebOutputRecord(string, bigDecimal, string1, date);

    }

    @Test
    public void givenNullHashcode_whenCalling_thenThrowNullPointerException() {
        String string = "null";
        BigDecimal bigDecimal = new BigDecimal("123");
        String string1 = "null";
        Date date = new Date();
        WebOutputRecord record = new WebOutputRecord(string, bigDecimal, string1, date);
        WebOutputRecord record1 = new WebOutputRecord(string, bigDecimal, string1, date);
        Assertions.assertTrue(record.hashCode() == record1.hashCode());
    }
    @Test
    public void givenEqualObjects_whenUsingOverrideEquals_thenReturnTrue(){
        String string = "null";
        BigDecimal bigDecimal = new BigDecimal("123");
        String string1 = "null";
        Date date = new Date();
        WebOutputRecord record = new WebOutputRecord(string, bigDecimal, string1, date);
        WebOutputRecord record1 =new WebOutputRecord(string, bigDecimal, string1, date);
        boolean equalObject = record.equals(record1);
        Assertions.assertTrue(equalObject);
    }
    @Test
    public void givenString_whenCallingSetFoundIn_thenReturnTrue(){
        String foundIn ="found in value is string";
        String string = "null";
        BigDecimal bigDecimal = new BigDecimal("123");
        String string1 = "null";
        Date date = new Date();
        WebOutputRecord record = new WebOutputRecord(string, bigDecimal, string1, date);
        record.setFoundIn(foundIn);
        Assertions.assertEquals(foundIn,record.getFoundIn());
    }
}
