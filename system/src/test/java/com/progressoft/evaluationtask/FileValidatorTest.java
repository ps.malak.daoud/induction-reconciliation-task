package com.progressoft.evaluationtask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.json.stream.JsonParsingException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidatorTest {

    @Test
    public void givenNullPathForFile_whenReadingFile_thenThrowNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new FileValidator(null));
        Assertions.assertEquals("null path", nullPointerException.getMessage());
    }


    @Test
    public void givenNonExistPathForFile_whenReadingFile_thenThrowFileNotFoundException() {
        Path sourcePath = Paths.get("test100");
        Assertions.assertThrows(NoFileFoundException.class, () -> new FileValidator(sourcePath));
    }


    @Test
    public void givenDirectoryForFile_whenReadingFile_thenThrowIllegalArgumentException() {
        Path path = Paths.get(".");
        Path targetPath = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            FileValidator fileValidator = new FileValidator(targetPath);
            fileValidator.dirValidator(path);
        });
        Assertions.assertEquals("Not file", exception.getMessage());
    }

    @Test
    public void givenNullFormatForFile_whenReadingFileBasedOnFileFormat_thenNullPointerException() throws IOException {
        Path sourcePath = Paths.get("../sample-files/input-files/bank-transactions.csv");
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> {
            FileValidator file = new FileValidator(sourcePath);
            boolean valid = file.isValidFormat(null);
            Assertions.assertFalse(valid);
        });
        Assertions.assertEquals("null file format", nullPointerException.getMessage());
    }

    @Test
    public void givenCVSFormat_whenReadingFileBasedOnFileFormat_thenReturnExpectedResult() throws NoFileFoundException {
        Path sourcePath = Paths.get("../sample-files/input-files/bank-transactions.csv");
        FileValidator parsingFile = new FileValidator(sourcePath);
        boolean valid = parsingFile.isValidFormat("CSV");
        Assertions.assertTrue(valid);

    }

    @Test
    public void givenJSONFormat_whenReadingFileBasedOnFileFormat_thenReturnExpectedResult() throws NoFileFoundException {
        Path targetPath = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        FileValidator parsingFile = new FileValidator(targetPath);
        boolean valid = parsingFile.isValidFormat("JSON");
        Assertions.assertTrue(valid);
    }

    @Test
    public void givenNullPath_whenCheckingCorrectFormat_thenThrowNullPointerException() {
        Path path = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> {
                    FileValidator fileValidator = new FileValidator(path);
                    fileValidator.isCorrect(null, "");
                });
        Assertions.assertEquals("null path", nullPointerException.getMessage());
    }

    @Test
    public void givenNullFormat_whenCheckingCorrectFormat_thenThrowNullPointerException() {
        Path path = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> {
                    FileValidator fileValidator = new FileValidator(path);
                    fileValidator.isCorrect(path, null);
                });
        Assertions.assertEquals("null format", nullPointerException.getMessage());
    }

    @Test
    public void givenUnknownFormat_whenCheckingCorrectFormat_thenThrowIllegalStateException() {
        Path path = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    FileValidator fileValidator = new FileValidator(path);
                    fileValidator.isCorrect(path, "null");
                });
        Assertions.assertEquals("Unknown format", illegalArgumentException.getMessage());
    }

    @Test
    public void givenCSVFormat_whenCheckingCorrectFileFormat_thenTrue() throws NoFileFoundException, IOException, ParseFormatException {
        Path path = Paths.get("../sample-files/input-files/bank-transactions.csv");
        FileValidator fileValidator = new FileValidator(path);
        Assertions.assertFalse(fileValidator.isCorrect(path, "CSV"));
    }

    @Test
    public void givenJSONFormat_whenCheckingFileCorrectFormat_thenTrue() throws NoFileFoundException, IOException, ParseFormatException {
        Path path = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        FileValidator fileValidator = new FileValidator(path);
        Assertions.assertTrue(fileValidator.isCorrect(path, "JSON"));
    }

    @Test
    public void givenUnknownFormat_whenCheckingCorrectFileFormat_thenThrowIllegalStateException() {
        Path path = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    FileValidator fileValidator = new FileValidator(path);
                    fileValidator.isCorrect(path, "iii");
                });
        Assertions.assertEquals("Unknown format", illegalArgumentException.getMessage());
    }
}
