package com.progressoft.evaluationtask;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class WebOutputRecord {

    @CsvBindByName(column = "found in file")
    private String foundIn;

    @CsvBindByName(column = "transaction id")
    private String transactionId;

    @CsvBindByName(column = "amount")
    private BigDecimal amount;

    @CsvBindByName(column = "currecny code")
    private String currency;

    @CsvBindByName(column = "value date")
    @CsvDate("yyyy-MM-dd")
    private Date date;

    public WebOutputRecord() {

    }

    public WebOutputRecord(String transactionId, BigDecimal amount, String currency, Date date) {
        if (transactionId == null && amount == null && currency == null && date == null)
            throw new NullPointerException("Null Data Entered");
        this.transactionId = transactionId;
        this.amount = amount;
        this.currency = currency;
        this.date = date;
    }

    public void setFoundIn(String foundIn) {
        this.foundIn = foundIn;
    }

    public String getFoundIn() {
        return foundIn;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebOutputRecord that = (WebOutputRecord) o;
        return Objects.equals(transactionId, that.transactionId) &&
                amount.compareTo(that.amount) == 0 &&
                Objects.equals(currency, that.currency) &&
                Objects.equals(date, that.date);
    }


    @Override
    public int hashCode() {
        return Objects.hash(transactionId, amount, currency, date);
    }

    private String convertDateToString(Date date) {
        String dateString = null;
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat sdfr = new SimpleDateFormat(pattern);
        try {
            dateString = sdfr.format(date);
        } catch (Exception ex) {
            ex.getMessage();
        }
        return dateString;
    }

    @Override
    public String toString() {
        String date = convertDateToString(getDate());
        return String.format(getFoundIn() + "," + getTransactionId() + "," + getAmount() + "," + getCurrency() + "," + date);

    }

    public String matchToString() {
        String date = convertDateToString(getDate());
        return String.format(getTransactionId() + "," + getAmount() + "," + getCurrency() + "," + date);

    }

}