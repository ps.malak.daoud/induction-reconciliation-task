package com.progressoft.evaluationtask;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;

public interface Parser {
    // TODO the path of the file should be passed to the read method //done
    HashMap<String, Record> read(Path path) throws IOException, ParseFormatException;
}
