package com.progressoft.evaluationtask;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;

public class OutputHandler implements OutputProducer {
    FileValidator fileValidator;
    File newMatchFile, newMisMatchFile, newMissingFile;

    public OutputHandler(File newMatchFile, File newMisMatchFile, File newMissingFile) throws IOException {
        this.newMatchFile = newMatchFile;
        this.newMisMatchFile = newMisMatchFile;
        this.newMissingFile = newMissingFile;
        writeFileHeaders();
    }

    @Override
    public void writeMatched(String match) {
        if (match == null) {
            throw new NullPointerException();
        }
        fileValidator = new FileValidator(Paths.get(String.valueOf(newMatchFile)));
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(newMatchFile, true))) {
            writer.append(match).append("\n");
            System.out.println("Writing Successful; Matched data at " + newMatchFile);
            writer.flush();
        } catch (IOException e) {
            throw new FileWritingException("failed to write matched", e);
        }
    }

    @Override
    public void writeUnmatched(String target, String source) {
        if (target == null && source == null) {
            throw new NullPointerException();
        }
        fileValidator = new FileValidator(Paths.get(String.valueOf(newMisMatchFile)));
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(newMisMatchFile, true))) {
            writer.append(target).append("\n");
            writer.append(source).append("\n");
            System.out.println("Writing Successful; Unmatched data at " + newMisMatchFile);
            writer.flush();
        } catch (IOException e) {
            throw new FileWritingException("failed to write mismatched", e);
        }

    }

    @Override
    public void writeMissing(String missing) {
        if (missing == null) {
            throw new NullPointerException();
        }
        fileValidator = new FileValidator(Paths.get(String.valueOf(newMissingFile)));
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(newMissingFile, true))) {
            writer.append(missing).append("\n");
            System.out.println("Writing Successful; Missing data at " + newMissingFile);
            writer.flush();
        } catch (IOException e) {
            throw new FileWritingException("failed to write missing", e);
        }
    }

    private void writeFileHeaders() throws IOException {
        String matchHeader = "transaction id,amount,currecny code,value date\n";
        String misHeader = "found in file,transaction id,amount,currecny code,value date\n";
        matchFileHeader(matchHeader);
        missMatchFileHeader(misHeader);
        missingFileHeader(misHeader);
    }

    private void missingFileHeader(String misHeader) throws IOException {
        FileWriter missingFileWriter = new FileWriter(newMissingFile);
        BufferedWriter missingBufferedWriter = new BufferedWriter(missingFileWriter);
        missingBufferedWriter.append(misHeader);
        missingBufferedWriter.flush();
        missingBufferedWriter.close();
    }

    private void missMatchFileHeader(String misHeader) throws IOException {
        FileWriter missMatchFileWriter = new FileWriter(newMisMatchFile);
        BufferedWriter missMatchedBufferedWriter = new BufferedWriter(missMatchFileWriter);
        missMatchedBufferedWriter.append(misHeader);
        missMatchedBufferedWriter.flush();
        missMatchedBufferedWriter.close();
    }

    private void matchFileHeader(String matchHeader) throws IOException {
        FileWriter matchFileWriter = new FileWriter(newMatchFile);
        BufferedWriter matchBufferedWriter = new BufferedWriter(matchFileWriter);
        matchBufferedWriter.append(matchHeader);
        matchBufferedWriter.flush();
        matchBufferedWriter.close();
    }
}
