package com.progressoft.evaluationtask;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import com.opencsv.bean.*;

public class CsvParser implements Parser {
    protected Path path;

    public CsvParser() throws NoFileFoundException {

    }

    @Override
    public HashMap<String, Record> read(Path path) throws IOException {
        new FileValidator(path);
        this.path = path;
        HashMap<String, Record> hashMap = new HashMap<>();
        try (BufferedReader bf = Files.newBufferedReader(path)) {
            csvFileObjectRead(hashMap, bf);
        }
        return hashMap;
    }

    private void csvFileObjectRead(HashMap<String, Record> hashMap, BufferedReader bf) {
        CsvToBean<Record> csvToBean = new CsvToBeanBuilder(bf)
                .withType(Record.class)
                .withIgnoreLeadingWhiteSpace(true)
                .withSkipLines(1)
                .build();
        for (Record record : csvToBean) {
            hashMap.put(record.getTransactionId(), record);
        }
    }

}
