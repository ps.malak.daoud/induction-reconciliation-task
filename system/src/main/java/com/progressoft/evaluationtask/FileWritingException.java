package com.progressoft.evaluationtask;

import java.io.IOException;

public class FileWritingException extends RuntimeException {
    public FileWritingException(String message, Exception e) {
        super(message,e);
    }
}
