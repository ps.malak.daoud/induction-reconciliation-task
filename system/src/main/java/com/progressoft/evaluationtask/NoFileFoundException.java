package com.progressoft.evaluationtask;

public class NoFileFoundException extends RuntimeException {
    public NoFileFoundException(String message) {
        super(message);
    }

}
