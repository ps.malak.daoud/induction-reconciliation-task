package com.progressoft.evaluationtask;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.*;
import java.text.*;
import java.util.*;

import javax.json.*;


public class JsonParser implements Parser {

    protected Path path;

    public JsonParser() throws NoFileFoundException {

    }

    @Override
    public HashMap<String, Record> read(Path path) throws IOException, ParseFormatException {
        new FileValidator(path);
        this.path = path;
        HashMap<String, Record> hashMap = new HashMap<>();
        try (InputStream fileInputStream = new FileInputStream(String.valueOf(path))) {
            try (JsonReader reader = Json.createReader(fileInputStream)) {
                jsonFileObjectRead(hashMap, reader);
            }
        }
        return hashMap;
    }

    private void jsonFileObjectRead(HashMap<String, Record> hashMap, JsonReader reader) throws ParseFormatException {
        JsonArray jsonArray = reader.readArray();
        List<Record> fileObjectsStream = JsonParser.jsonMapping(jsonArray);
        for (Record record : fileObjectsStream) {
            hashMap.put(record.getTransactionId(), record);
        }
    }


    public static List<Record> jsonMapping(JsonArray jsonArray) throws ParseFormatException {
        List<Record> fileObjectsList = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = jsonArray.getJsonObject(i);
            Record record = setGetFileObject(jsonObject);
            fileObjectsList.add(record);
        }
        return fileObjectsList;
    }

    private static Record setGetFileObject(JsonObject jsonObject) throws ParseFormatException {
        Record record = new Record();
        record.setTransactionId(jsonObject.getString("reference"));
        record.setAmount(new BigDecimal(jsonObject.getString("amount")));
        record.setCurrency(jsonObject.getString("currencyCode"));
        record.setDate(mapDateFormat(jsonObject.getString("date")));
        return record;
    }

    private static Date mapDateFormat(String strDate) throws ParseFormatException {
        SimpleDateFormat fromFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date tempDate = null;
        Date parse = null;
        try {
            tempDate = fromFormat.parse(strDate);
            String stringDate = toFormat.format(tempDate);
            parse = toFormat.parse(stringDate);

        } catch (ParseException e) {
            throw new ParseFormatException("Parsing Date to String Exception", e);
        }
        return parse;
    }

}