package com.progressoft.evaluationtask;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class CsvToJson {

    public String convert(String inputPath){
        File inputFile = new File(inputPath);
        String outputPath = inputPath.split("[.]")[0]+".json";
        File output = new File(outputPath);
        List<Map<?, ?>> data = null;
        try {
            data = readObjectsFromCsv(inputFile);
            writeAsJson(data, output);
            return outputPath;
        } catch (IOException e) {
            throw new IllegalArgumentException("Can not find CSV file");
        }
    }

    public List<Map<?, ?>> readObjectsFromCsv(File file) throws IOException {
        CsvSchema bootstrap = CsvSchema.emptySchema().withHeader();
        CsvMapper csvMapper = new CsvMapper();
        MappingIterator<Map<?, ?>> mappingIterator = csvMapper.reader(Map.class).with(bootstrap).readValues(file);

        return mappingIterator.readAll();
    }

    public void writeAsJson(List<Map<?, ?>> data, File file) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(file, data);
    }
}
