package com.progressoft.evaluationtask;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ParseCSVasList {
    public ParseCSVasList() {
    }

    public List<WebOutputRecord> parseCSVasList(Path path) throws ParseFormatException, IOException {
        try (BufferedReader br = Files.newBufferedReader(path,
                StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<WebOutputRecord> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(WebOutputRecord.class);

            CsvToBean csvToBean = new CsvToBeanBuilder(br)
                    .withType(WebOutputRecord.class)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            return csvToBean.parse();
        }
    }
}
