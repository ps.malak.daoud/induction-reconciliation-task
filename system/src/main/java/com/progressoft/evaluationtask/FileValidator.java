package com.progressoft.evaluationtask;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class FileValidator {

    private List<String> CSVCorrectFormatList = new ArrayList<>();

    private static String CSV = "CSV";
    private static String JSON = "JSON";

    public FileValidator(Path sourceFile) throws NoFileFoundException {
        if (sourceFile == null)
            throw new NullPointerException("null path");
        isValidPath(sourceFile);
        dirValidator(sourceFile);

    }

    public boolean isCorrect(Path path, String pathFormat) throws NoFileFoundException, IOException, ParseFormatException {
        isNull(path, pathFormat);
        unknownPathFormat(pathFormat);
        Boolean csvEquals = true;
        Boolean csvEquals1 = csvCorrectFormat(path, pathFormat);
        if (csvEquals1 != null)
            return csvEquals1;
        jsonCorrectFormat(path, pathFormat);
        return true;
    }

    private void unknownPathFormat(String pathFormat) {
        if (!pathFormat.equalsIgnoreCase(CSV) && !pathFormat.equalsIgnoreCase(JSON)) {
            throw new IllegalArgumentException("Unknown format");
        }
    }

    private void isNull(Path path, String pathFormat) {
        if (path == null) {
            throw new NullPointerException("null path");
        }
        if (pathFormat == null) {
            throw new NullPointerException("null format");
        }
    }

    private static void jsonCorrectFormat(Path path, String pathFormat) throws NoFileFoundException, IOException, ParseFormatException {
        if (pathFormat.equalsIgnoreCase(JSON)) {
            JsonParser jsonFileParsing = new JsonParser();
            HashMap<String, Record> list = jsonFileParsing.read(path);
        }
    }

    // TODO true or false why using null
    private Boolean csvCorrectFormat(Path path, String pathFormat) {
        csvCorrectFileFormat();
        if (pathFormat.equalsIgnoreCase(CSV)) {
            boolean csvEquals;
            try {
                CsvParser csvParsing = new CsvParser();
                HashMap<String, Record> list = csvParsing.read(path);
                csvEquals = list.equals(CSVCorrectFormatList);
            }
            // TODO useless exception handling //done
            catch (IOException e) {
                throw new ParseFormatException("CSV parsing error", e);
            }
            return csvEquals;
        }
        return null;
    }

    private void csvCorrectFileFormat() {
        CSVCorrectFormatList.add("trans unique id");
        CSVCorrectFormatList.add("trans description");
        CSVCorrectFormatList.add("amount");
        CSVCorrectFormatList.add("currecny");
        CSVCorrectFormatList.add("purpose");
        CSVCorrectFormatList.add("value date");
        CSVCorrectFormatList.add("trans type");
    }

    private void isValidPath(Path source) throws NoFileFoundException {
        Objects.requireNonNull(source, "null source path");
        if (Files.notExists(source))
            throw new NoFileFoundException("There was an exception!  The file was not found!");
        if (Files.isDirectory(source))
            throw new IllegalArgumentException("Not file");
    }


    public void dirValidator(Path sourcePath) {
        Objects.requireNonNull(sourcePath, "null path");
        if (Files.isDirectory(sourcePath))
            throw new IllegalArgumentException("Not file");
    }

    public boolean isValidFormat(String sourceFormat) {
        if (sourceFormat == null)
            throw new NullPointerException("null file format");
        validFormatForFiles(sourceFormat);
        invalidFileFormat(sourceFormat);
        return true;
    }

    private void invalidFileFormat(String sourceFormat) {
        unknownPathFormat(sourceFormat);
    }

    private boolean validFormatForFiles(String sourceFormat) {
        return sourceFormat.equalsIgnoreCase(CSV) || sourceFormat.equalsIgnoreCase(JSON);
    }

}


