package com.progressoft.evaluationtask;

public class ParseFormatException extends RuntimeException {
    public ParseFormatException(String message , Throwable e) {
        super(message , e);
    }
}
