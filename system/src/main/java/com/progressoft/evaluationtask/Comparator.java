package com.progressoft.evaluationtask;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Objects;

public class Comparator {
    // TODO this should be injected //done
    OutputProducer OutputProducer;
    Parser source;
    Parser target;

    public Comparator(Parser source, Parser target, OutputProducer outputProducer) {
        if (source == null && target == null)
            throw new NullPointerException("null source and target Parsers");
        this.OutputProducer = outputProducer;
        this.source = source;
        this.target = target;
    }

    public void compare(Path sourcePath, Path targetPath) throws IOException, ParseFormatException {
        HashMap<String, Record> sourceRecords = source.read(sourcePath);
        HashMap<String, Record> targetRecords = target.read(targetPath);
        forCompare(sourceRecords, targetRecords);
        System.out.println("Reconciliation finished.");
//        System.out.println("Result files are available in /Phase-One-Evaluation-Task/reconciliation-results");
    }

    private void forCompare(HashMap<String, Record> sourceList, HashMap<String, Record> targetList) throws IOException {
        Record sourceRecord;

        for (String key : targetList.keySet()) {
            Record targetRecord = targetList.get(key);
            sourceRecord = sourceList.get(key);
            if (isMissing(sourceRecord)) {
                // TODO this should be the SOURCE //done
                writeToMissing(targetRecord, "SOURCE");
                System.out.println("missing in source : "+targetRecord);
                continue;
            }
            // TODO enhance this method by passing the targetRecord object and source obj //done
            if (sourceRecord.equals(targetRecord)) {
                writeToMatched(targetRecord);
                System.out.println("matched files : "+targetRecord);
            } else {
                writeToUnMatch(sourceRecord, targetRecord);
                System.out.println("unmatched files : "+ sourceRecord + " " +targetRecord);
            }
            sourceList.remove(sourceRecord.getTransactionId());
        }
    }

    private boolean isMissing(Record record) {
        return Objects.isNull(record);
    }

    private void writeToUnMatch(Record targetRecord, Record sourceRecord) throws IOException {
        sourceRecord.setFoundIn("SOURCE");
        targetRecord.setFoundIn("TARGET");
        OutputProducer.writeUnmatched(targetRecord.toString(), sourceRecord.toString());
    }

    private void writeToMissing(Record record, String source) {
        record.setFoundIn(source);
        OutputProducer.writeMissing(record.toString());
    }

    private void writeToMatched(Record record) {
        OutputProducer.writeMatched(record.matchToString());
    }

}
