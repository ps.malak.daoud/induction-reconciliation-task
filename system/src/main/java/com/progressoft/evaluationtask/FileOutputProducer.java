package com.progressoft.evaluationtask;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileOutputProducer implements OutputProducer {
    // TODO you are not writing the headers //done
    // TODO accept the root directory where I want to write the result //done
    FileValidator fileValidator;
    // TODO initialization should be part of the constructor logic //done
    String tempDirectoryPath ;
    File matchFile, misMatchFile, missingFile;
    boolean match, misMatch, missing;

    public FileOutputProducer() {
        try {
            tempDirectoryPath =System.getProperty("user.home") + File.separator + "console-reconciliation-results";
            Path dirPath = Paths.get(tempDirectoryPath);
            validateDirPath(String.valueOf(dirPath));
            writingFilesPath(dirPath);
            createWritingFiles();
            writeFileHeaders();
        } catch (IOException e) {
            throw new NoFileFoundException("Error while validating Directory or File paths");
        }
    }

    public void writeMatched(String match) {
        if (match == null) {
            throw new NullPointerException();
        }
        fileValidator = new FileValidator(Paths.get(String.valueOf(matchFile)));
        // TODO absolute paths //done
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(matchFile, true))) {
            writer.append(match).append("\n");
            System.out.println("Writing Successful; Matched data at " + matchFile);
            writer.flush();
        } catch (IOException e) {
            throw new FileWritingException("failed to write matched", e);
        }
    }

    public void writeUnmatched(String target, String source) {
        if (target == null && source == null) {
            throw new NullPointerException();
        }
        fileValidator = new FileValidator(Paths.get(String.valueOf(misMatchFile)));
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(misMatchFile, true))) {
            writer.append(target).append("\n");
            writer.append(source).append("\n");
            System.out.println("Writing Successful; Unmatched data at " + misMatchFile);
            writer.flush();
        } catch (IOException e) {
            throw new FileWritingException("failed to write mismatched", e);
        }

    }

    public void writeMissing(String missing) {
        if (missing == null) {
            throw new NullPointerException();
        }
        fileValidator = new FileValidator(Paths.get(String.valueOf(missingFile)));
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(missingFile, true))) {
            writer.append(missing).append("\n");
            System.out.println("Writing Successful; Missing data at " + missingFile);
            writer.flush();
        } catch (IOException e) {
            throw new FileWritingException("failed to write missing", e);
        }
    }

    private void createWritingFiles() throws IOException {
        if (!(matchFile.exists()) || !(misMatchFile.exists()) || !(missingFile.exists())) {
            this.match = matchFile.createNewFile();
            this.misMatch = misMatchFile.createNewFile();
            this.missing = missingFile.createNewFile();
        }
    }

    private void writingFilesPath(Path dirPath) {
        try {
            this.matchFile = File.createTempFile("Matching-Transactions-file", ".csv", new File(String.valueOf(dirPath)));
            this.misMatchFile = File.createTempFile("Mismatching-Transactions-file", ".csv", new File(String.valueOf(dirPath)));
            this.missingFile = File.createTempFile("Missing-Transactions-file", ".csv", new File(String.valueOf(dirPath)));

        } catch (IOException e) {
            throw new FileWritingException("failed to create temp", e);
        }
    }

    private void validateDirPath(String uploadFolder) throws IOException {
        if (!Files.exists(Paths.get(uploadFolder))) {
            Files.createDirectory(Paths.get(uploadFolder));
        }
    }

    private void writeFileHeaders() throws IOException {
        String matchHeader = "transaction id,amount,currecny code,value date\n";
        String misHeader = "found in file,transaction id,amount,currecny code,value date\n";
        matchFileHeader(matchHeader);
        missMatchFileHeader(misHeader);
        missingFileHeader(misHeader);
    }

    private void missingFileHeader(String misHeader) throws IOException {
        FileWriter missingFileWriter = new FileWriter(missingFile);
        BufferedWriter missingBufferedWriter = new BufferedWriter(missingFileWriter);
        missingBufferedWriter.append(misHeader);
        missingBufferedWriter.flush();
        missingBufferedWriter.close();
    }

    private void missMatchFileHeader(String misHeader) throws IOException {
        FileWriter missMatchFileWriter = new FileWriter(misMatchFile);
        BufferedWriter missMatchedBufferedWriter = new BufferedWriter(missMatchFileWriter);
        missMatchedBufferedWriter.append(misHeader);
        missMatchedBufferedWriter.flush();
        missMatchedBufferedWriter.close();
    }

    private void matchFileHeader(String matchHeader) throws IOException {
        FileWriter matchFileWriter = new FileWriter(matchFile);
        BufferedWriter matchBufferedWriter = new BufferedWriter(matchFileWriter);
        matchBufferedWriter.append(matchHeader);
        matchBufferedWriter.flush();
        matchBufferedWriter.close();
    }
}

