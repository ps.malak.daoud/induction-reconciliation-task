package com.progressoft.evaluationtask;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ServiceLoader;

public class Factory {
    private static final String JSON = "json";
    private static final String CSV = "csv";
    private static HashMap<String, ParserFactory> parserFactories = new HashMap<>();

    static {
        parserFactories.put(JSON, Factory::getJsonParser);
        parserFactories.put(CSV, Factory::getCsvParser);
    }

    public static Parser factory(String format) throws NoFileFoundException {
        if (format == null)
            throw new NullPointerException("null file format");
        //Null Object Factory
        ParserFactory parser =
                parserFactories.getOrDefault(format.toLowerCase(), () ->
                {
                    throw new IllegalArgumentException("invalid format");
                });

        return parser.newParser();
    }

    private static CsvParser getCsvParser() {
        return new CsvParser();
    }

    private static JsonParser getJsonParser() {
        return new JsonParser();
    }

    interface ParserFactory {
        Parser newParser();
    }
}
