package com.progressoft.evaluationtask;

public interface OutputProducer {
    void writeMatched(String match);
    void writeUnmatched(String unmatch, String unmatch1);
    void writeMissing(String missing);
}
